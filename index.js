module.exports = {
    S3: require('./src/AWS/S3'),
    SQS: require('./src/AWS/SQS'),
    SNS: require('./src/AWS/SNS'),
    SSM: require('./src/AWS/SSM'),
    Dynamo: require('./src/AWS/Dynamo'),
    Logger: require('./src/Logger'),
    MySQL: require('./src/MySQL'),
    Request: require('./src/Request')
};