const axios = require('axios');

class Request {
    constructor(logger) {
        this.logger = logger;
    }

    async get(url, params, raw) {
        return await new Promise(resolve => {
            axios.get(url, params)
              .then(function (response) {
                  if(raw) {
                      return resolve(response);
                  }
                  return resolve(response.data);
              })
              .catch(function (error) {
                resolve(false)
              });  
        })
    }

}

module.exports = Request;
