const AWS = require('aws-sdk');

class SSM {

    constructor({ logger, region }) {
        this.client = new AWS.SSM({ region: this.getRegion(region) });
        this.logger = logger;
    }

    getRegion(region) {
        return region || process.env.SSM_REGION;
    }

    getPath(path) {
        return path || process.env.SSM_DEFAULT_PATH;
    }

    async getParametersByPath(path) {

        path = this.getPath(path);

        const ssmLookupResult = await new Promise(resolve => {
            this.client.getParametersByPath({ Path: path }, (err, data) => {
                if(err) {
                    console.log(err);
                    this.logger.log({ type: 'param_store_lookup_failed', info: err });
                    return resolve(false);
                }
                return resolve(data.Parameters);
            });
        });
        
        if(!ssmLookupResult) {
            return false;
        }

        const params = {};
        for(var param of ssmLookupResult) {
            params[param.Name.replace(path, '')] = param.Value;
        }

        return params;
    }
}

module.exports = SSM;
