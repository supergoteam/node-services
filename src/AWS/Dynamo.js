const AWS = require('aws-sdk');

class Dynamo {

    constructor (region, logger, table) {
        this.docClient = new AWS.DynamoDB.DocumentClient({ region });
        this.logger = logger;
        this.table = (table !== undefined) ? table : process.env.DYNAMO_TABLE;
    }

    getTable (table) {
        return  (table !== undefined) ? table : this.table;
    }

    async get(key, table) {
        const tableName = this.getTable(table);
        var params = {
            TableName: tableName,
            Key:{
                "id": key.toString()
            }
        };
        this.logger.logTime();
        return await new Promise(resolve => {
            this.docClient.get(params, ((resolve, err, data) => {
                if (err) {
                    this.logger.log({ type: "dynamo_read_failed", info: err});
                    resolve(false);
                } else {
                    const timeTaken = this.logger.logTime();
                    this.logger.log({ type: "dynamo_read_success", info: { timeTaken, data }});
                    if(data.Item) {
                        return resolve(data.Item);
                    }
                    resolve(false);
                }
            }).bind(this, resolve));
        });
    }

    async put (item, table) {
        const tableName = this.getTable(table);

        var params = {
            TableName: tableName,
            Item:item
        };

        return await new Promise(resolve => {
            this.docClient.put(params, ((resolve, err, data) => {
                if (err) {
                    this.logger.log({ type: "dynamo_write_failed", info: err});
                    resolve(false);
                } else {
                    const timeTaken = this.logger.logTime();
                    this.logger.log({ type: "dynamo_write_success", info: { timeTaken, data }});
                    resolve(data);
                }
            }).bind(this, resolve))
        });
    };

    async incrementCounter(key, countColumn, table) {
        const tableName = this.getTable(table);
        var params = {
            TableName:tableName,
            Key:{
                "id": key.toString()
            },
            UpdateExpression: `set ${countColumn} = ${countColumn} + :val`,
            ExpressionAttributeValues:{
                ":val": 1
            },
            ReturnValues:"UPDATED_NEW"
        };
        return await new Promise(resolve => {
            this.docClient.update(params, ((resolve, err, data) => {
                if (err) {
                    this.logger.log({ type: "dynamo_increment_failed", info: err});
                    resolve(false);
                } else {
                    this.logger.log({ type: "dynamo_increment_success" });
                    resolve(true);
                }
            }).bind(this, resolve));
        });
    }
}

module.exports = Dynamo;