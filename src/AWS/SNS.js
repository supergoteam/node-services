const AWS = require('aws-sdk');

class SNS {

    constructor({ logger, region }) {
        this.client = new AWS.SNS({ region: this.getRegion(region) });

        this.logger = logger;
    }

    getRegion(region) {
        return region || process.env.SNS_REGION;
    }

    getTopicArn(topicArn) {
        return topicArn || process.env.SNS_DEFAULT_TOPIC_ARN;
    }

    async publish ({ subject, message, topicArn }) {

        if(typeof message === 'string') {
            message = message.trim();
        } else {
            Object.keys(message).forEach(index => message[index] = message[index].trim());
        }

        var messageStructure = '';
        if(typeof message !== 'string') {
            message = JSON.stringify(message);
            messageStructure = 'json'
        }

        const params = {
            Subject: subject.trim(),
            TopicArn: this.getTopicArn(topicArn),
            Message: message,
            MessageStructure: messageStructure
        };

        this.logger.log({ type: 'sending_sns', info: params })

        try {
            await this.client.publish(params).promise();
            return true;
        }
        catch(e) {
            console.log(e);
            this.logger.log({ type: 'unhandled_sns_error', info: e });
            this.logger.log({ type: 'sns_publish_error', info: { errorMsg: e.message }});
            return false;
        }
    }

}

module.exports = SNS;