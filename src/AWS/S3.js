const AWS = require('aws-sdk');

class S3 {

    constructor({ logger }) {
        this.client = new AWS.S3();
        this.logger = logger;
    }

    async put (bucket, key, data) {

        const params = { Bucket: bucket, Key: key, Body: data };

        return await new Promise((resolve) => {
            this.client.putObject(params, (err,data) => {
                if(err) {
                    this.logger.log({ type: 's3_write_error', info: err });
                    resolve(false);
                }
                if(data) {
                    this.logger.log({ type: 's3_write_success', info: { ETag: data.ETag }});
                    resolve(data);
                }
            });
        });
    }

    async get (bucket, key, returnType) {
    
        const params = { Bucket: bucket, Key: key };
    
        return await new Promise((resolve) => {

            this.logger.logTime();
            this.client.getObject(params, (err,data) => {

                if(err) {
                    this.logger.log({ type: 's3_read_error', info: err });
                    resolve(false);
                }

                if(data) {
                    this.logger.log({ type: 's3_read_success', info: { timeTaken: this.logger.logTime(), length: data.length }});
                    if(returnType == 'raw') {
                        resolve(data.Body);
                    } else {
                        const returnData = data.Body.toString('utf-8');
                        resolve(returnData);
                    }
                }
            });
        });
        
    }

    async delete (bucket, key) {
    
        const params = { Bucket: bucket, Key: key };
    
        return await new Promise((resolve) => {

            this.logger.logTime();
            this.client.deleteObject(params, (err,data) => {

                if(err) {
                    this.logger.log({ type: 's3_delete_error', info: err });
                    resolve(false);
                }

                if(data) {
                    this.logger.log({ type: 's3_delete_success', info: { timeTaken: this.logger.logTime(), length: data.length }});
                    resolve(true);
                }
            });
        });
        
    }
    
    async listBucket (params, isValidKey, isValidTags, isValidHead, batchLimit) {
        isValidKey = isValidKey || (() => true);
        isValidTags = isValidTags || (() => true);
        isValidHead = isValidHead || (() => true);
        batchLimit = batchLimit || 0;
        
        return await new Promise((resolve) => {
            this.logger.logTime();
            const callback = (s3Objects) => {
                const timeTaken = this.logger.logTime();
                this.logger.log({ type: 's3_list_time', info: timeTaken });
                resolve(s3Objects);
            };
            this.listAllObjects(params, false, callback, isValidKey, isValidTags, isValidHead, [], batchLimit);
        });
    }

    listAllObjects (params, token, callback, isValidKey, isValidTags, isValidHead, s3Objects, batchLimit) {
        if(token) params.ContinuationToken = token;
    
        this.logger.logTime();
        this.client.listObjectsV2(params, async(err, data) => {
    
            // this.logger.log({ type: 'object_fetch', info: { total: data.Contents.length, time: this.logger.logTime() }});
            const validKeys = data.Contents.filter((obj) => isValidKey(obj.Key));
    
            const validObjects = [];
            for(var i = 0; i < validKeys.length; i++) {
                //this.logger.logTime();
                const tags = true; //await new Promise(resolve => this.client.getObjectTagging({ Bucket: params.Bucket, Key: validKeys[i].Key }, (err, data) => resolve(data)));
                //this.logger.log({type: 'tag_fetch_time', info: this.logger.logTime()})
                if(!tags) {
                    this.logger.log({ 'type': 'permissions_error', info: { message: 'Permissions for getObjectTagging not set.'}});
                    process.exit();
                }
                if(tags === true || isValidTags(tags)) {
                    //this.logger.logTime();
                    const head = true; // disabled to speed up selection.await new Promise(resolve => this.client.headObject({ Bucket: params.Bucket, Key: validKeys[i].Key }, (err, data) => resolve(data)));
                    //this.logger.log({type: 'head_fetch_time', info: this.logger.logTime()})
                    if(head === true || isValidHead(head)) {
                        validObjects.push(validKeys[i]);
                    }
                }
            }
    
            s3Objects = s3Objects.concat(validObjects);
    
            if(s3Objects.length > 0 || Math.floor(Math.random()*100) == 1) {
                this.logger.log({ type: 'object_accepted', info: { total: s3Objects.length, batchLimit: batchLimit }});
            }
    
            if(!data.IsTruncated || (batchLimit != 0 && s3Objects.length >= batchLimit)) {
                if(batchLimit > 0) {
                    s3Objects = s3Objects.slice(0,batchLimit);
                }
                callback(s3Objects);
                return;
            }

            listAllObjects(params, data.NextContinuationToken, callback, isValidKey, isValidTags, isValidHead, s3Objects);

        });
    }

}

module.exports = S3;