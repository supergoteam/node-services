const AWS = require('aws-sdk');

class SQS {

    constructor({ logger, region }) {
        this.client = new AWS.SQS({ region: this.getRegion(region) });
        this.logger = logger;
    }

    getRegion(region) {
        return region || process.env.SQS_REGION;
    }

    getQueueUrl(queueUrl) {
        return queueUrl || process.env.SQS_DEFAULT_QUEUE_URL;
    }

    async sendMessage ({ message, messageGroupId, queueUrl }) {

        return await new Promise(resolve => {

            if(!(typeof message == 'string')) {
                message = JSON.stringify(message);
            }

            const params = {
                QueueUrl: this.getQueueUrl(queueUrl),
                MessageBody: message,
            }

            if(messageGroupId) {
                params.MessageGroupId = messageGroupId;
            }

            this.logger.log({ type: 'sqs_send_message', info: params });

            this.client.sendMessage(params, (err, data) => {
                if (err) {
                    this.logger.log({ type: 'sqs_write_error', info: err });
                    resolve(false);
                }
                if (data) {
                    this.logger.log({ type: 'sqs_write_success', info: data });
                    resolve(data.MessageId);
                }
            });
        });
    }

    async receiveMessage({ numberOfMessages, visibilityTimeout, queueUrl }) {

        return await new Promise(resolve => {
            const params = {
                QueueUrl: this.getQueueUrl(queueUrl),
            };

            if(numberOfMessages) {
                params.MaxNumberOfMessages = numberOfMessages;
            }

            if(visibilityTimeout) {
                params.VisibilityTimeout = visibilityTimeout;
            }

            this.client.receiveMessage(params, (err, data) => {
                if(err) {
                    this.logger.log({ type: 'sqs_receive_message_error', err });
                    resolve(false);
                }
                if(!data.Messages || data.Messages.length == 0) {
                    this.logger.log({ type: 'sqs_no_messages', err });
                    resolve(false);
                }
                
                resolve(data);
            });
        });
    }

    async deleteMessage({ receiptHandle, queueUrl }) {

        return await new Promise(resolve => {
            const params = {
                QueueUrl: this.getQueueUrl(queueUrl),
                ReceiptHandle: receiptHandle
            };

            this.client.deleteMessage(params, (function(err, data) {
                if(err) {
                    this.logger.log({ type: 'sqs_delete_error', info: err });
                    return resolve(false);
                }
                return resolve(true);
            }).bind(this));
        });

    }
}

module.exports = SQS;