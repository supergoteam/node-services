var mysql = require('mysql');

class MySQL {

    constructor(logger) {
        this.connections = {};
        this.setLogger(logger);
    }

    async init(logger) {
        this.setLogger(logger);
        if(process.env.MYSQL_HOST) {
            this.logger.log({ type: 'create_default_connection' });
            await this.addConnection('default', process.env.MYSQL_HOST, process.env.MYSQL_PORT, process.env.MYSQL_USER, process.env.MYSQL_PASS, process.env.MYSQL_DATABASE);
        }
        return this;
    }

    setLogger(logger) {
        this.logger = logger;
    }

    async addConnection(name, host, port, user, password, database) {
    
        this.logger.log({ type: 'create_db_connection' });
        
        const newConnection = mysql.createConnection({ host, port, user, password, database });
    
        const connSuccess = await new Promise((resolve) => {
            newConnection.connect((function(err) {
                if(err) {
                    this.logger.log({ type: 'create_db_failed', info: err });
                    resolve(false);
                } else {
                    this.logger.log({ type: 'create_db_success' });
                    resolve(true);
                }
            }).bind(this));
        });
        
        if(connSuccess === false) {
            return false;
        }

        this.connections[name] = newConnection;
        return true;
    }

    async hasConnection(connection, callback) {
        connection = connection || 'default';
        if(!this.connections[connection]) {
            this.logger.log({ type: 'invalid_connection', info: { connections: this.connections, connection }});
            return false;
        }
        return await callback(this.connections[connection]);
    }

    async query ({ connection, sql, values, bulk }) {

        this.logger.log({ type: 'run_query', info: { sql, values: (bulk ? values[0].length : values) }});

        return this.hasConnection(connection, async connection => await new Promise(resolve => {
            this.logger.logTime();
            connection.query({sql: sql, values: values},  ((error, results, fields) => {
                const timeTaken = this.logger.logTime();
                if(error) {
                    this.logger.log({ type: 'query_error', info: error });
                    resolve(false);
                } else {
                    this.logger.log({ type: 'query_time', info: { sql, timeTaken, rows: results.affectedRows }});
                    resolve(results);
                }
            }));
        }));
    }

    async startTransaction ({ connection }) {
    
        this.logger.log({ type: 'initiating_transaction' });
        return this.hasConnection(connection, async connection => await new Promise(resolve => {
            connection.beginTransaction(
                async (err) => {
                    if (err) { 
                        this.logger.log({ type: 'transaction_start_failed', info: err});
                        resolve(false); 
                    }
                    resolve(true);
                }
            );
        }));
    }
        
    async rollback ({ connection, info }) {
        this.logger.log({ type: 'initiating_rollback', info });
        return this.hasConnection(connection, async connection => await new Promise((resolve) => {
            connection.rollback((function() {
                this.logger.log({ type: 'rollback_complete' });
                return resolve(true);
            }).bind(this));
        }));
    };
    
    async commit ({ connection }) {
        return this.hasConnection(connection, async connection => await new Promise((resolve) => {
            connection.commit((async function(err) {
                if (err) {
                    const info = { type: 'commit' };
                    await rollback({ connection, info });
                    return resolve(false);
                }
                this.logger.log({ type: 'import_committed'});
                return resolve(true);
            }).bind(this));
        }));
    };
}

module.exports = MySQL;