class Logger {
    constructor(requestId, silent) {
        this.time = new Date().getTime();
        this.maxMemoryUsed = 0;
        this.requestId = requestId;
        this.silent = silent || false;
    }
    logTime() {
        const diff = new Date().getTime() - this.time;
        this.time = new Date().getTime();
        return diff;
    }
    log(msg, data) {
        const logData = (data == undefined) ? msg : { type: msg, info: data }
        const logMessage = { requestId: this.requestId, ...logData };
        if(!this.silent) {
            console.debug(JSON.stringify(logMessage));
        }
    }
    
    memoryCheck() { 
        const currentMemory = Math.round(process.memoryUsage().heapUsed/1024/1024);
        if(currentMemory > this.maxMemoryUsed) {
            this.maxMemoryUsed = currentMemory;
        }
        return this.maxMemoryUsed;
    }
}

module.exports = Logger;
